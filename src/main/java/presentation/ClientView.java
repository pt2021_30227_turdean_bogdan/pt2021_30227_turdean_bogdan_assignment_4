package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ClientView extends JFrame {
    private JButton viewAll = new JButton("View All");
    private JButton createOrder = new JButton("Create order");
    private JButton search = new JButton("Search");

    private JTextField id = new JTextField(10);
    private JTextField nume = new JTextField(30);
    private JTextField rating = new JTextField(10);
    private JTextField calories = new JTextField(10);
    private JTextField proteins = new JTextField(10);
    private JTextField fats = new JTextField(10);
    private JTextField sodium = new JTextField(10);
    private JTextField price = new JTextField(20);

    private JTable table;

    public ClientView(){
        JPanel titlu = new JPanel();
        titlu.setLayout(new FlowLayout());
        titlu.add(new JLabel("Client"));

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.X_AXIS));
        butoane.add(viewAll);
        butoane.add(createOrder);
        butoane.add(search);

        JPanel text1 = new JPanel();
        text1.setLayout(new BoxLayout(text1, BoxLayout.X_AXIS));
        text1.add(new JLabel("ID"));
        text1.add(id);

        JPanel text2 = new JPanel();
        text2.setLayout(new BoxLayout(text2, BoxLayout.X_AXIS));
        text2.add(new JLabel("Title"));
        text2.add(nume);

        JPanel text3 = new JPanel();
        text3.setLayout(new BoxLayout(text3, BoxLayout.X_AXIS));
        text3.add(new JLabel("Rating"));
        text3.add(rating);

        JPanel text4 = new JPanel();
        text4.setLayout(new BoxLayout(text4, BoxLayout.X_AXIS));
        text4.add(new JLabel("Calories"));
        text4.add(calories);

        JPanel text5 = new JPanel();
        text5.setLayout(new BoxLayout(text5, BoxLayout.X_AXIS));
        text5.add(new JLabel("Proteins"));
        text5.add(proteins);

        JPanel text6 = new JPanel();
        text6.setLayout(new BoxLayout(text6, BoxLayout.X_AXIS));
        text6.add(new JLabel("Fats"));
        text6.add(fats);

        JPanel text7 = new JPanel();
        text7.setLayout(new BoxLayout(text7, BoxLayout.X_AXIS));
        text7.add(new JLabel("Sodium"));
        text7.add(sodium);

        JPanel text8 = new JPanel();
        text8.setLayout(new BoxLayout(text8, BoxLayout.X_AXIS));
        text8.add(new JLabel("Price"));
        text8.add(price);

        JPanel panelTabel = new JPanel();
        panelTabel.setLayout(new FlowLayout());
        String data[][] = new String [100000][8];
        String column[] = {"ID", "Title", "Rating", "Calories", "Proteins", "Fats", "Sodium", "Price"};
        table = new JTable(data, column);
        JScrollPane pane = new JScrollPane(table);
        panelTabel.add(pane);

        JPanel last = new JPanel();
        last.setLayout(new BoxLayout(last, BoxLayout.Y_AXIS));
        last.add(titlu);
        last.add(butoane);
        last.add(text1);
        last.add(text2);
        last.add(text3);
        last.add(text4);
        last.add(text5);
        last.add(text6);
        last.add(text7);
        last.add(text8);
        last.add(panelTabel);

        this.setContentPane(last);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle("Clients Window");
    }

    public void reset(){
        id.setText("");
        nume.setText("");
        calories.setText("");
        rating.setText("");
        proteins.setText("");
        fats.setText("");
        sodium.setText("");
        price.setText("");
    }

    public void addViewAllListener(ActionListener mal){
        viewAll.addActionListener(mal);
    }

    public void addSearchListener(ActionListener mal){
        search.addActionListener(mal);
    }

    public void addCreateOrderListener(ActionListener mal){
        createOrder.addActionListener(mal);
    }

    public JTable getTable() {
        return this.table;
    }

    public int getId() {
        try {
            return Integer.parseInt(id.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti un numar in campul ID");
        }
        return -1;
    }

    public void setId(String id) {
        this.id.setText(id);
    }

    public String getName() {
        return nume.getText();
    }

    public void setName(String title) {
        this.nume.setText(title);
    }

    public double getRating() {
        try{
            return Double.parseDouble(rating.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti un numar pentru campl rating");
        }
        return -1;
    }

    public void setRating(String rating) {
        this.rating.setText(rating);
    }

    public int getCalories() {
        try {
            return Integer.parseInt(calories.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul calorii");
        }
        return -1;
    }

    public void setCalories(String calories) {
        this.calories.setText(calories);
    }

    public int getProteins() {
        try {
            return Integer.parseInt(proteins.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul proteine");
        }
        return -1;
    }

    public void setProteins(String proteins) {
        this.proteins.setText(proteins);
    }

    public int getFats() {
        try {
            return Integer.parseInt(fats.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul grasimi");
        }
        return -1;
    }

    public void setFats(String fats) {
        this.fats.setText(fats);
    }

    public int getSodium() {
        try {
            return Integer.parseInt(sodium.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul sodiu");
        }
        return -1;
    }

    public void setSodium(String sodium) {
        this.sodium.setText(sodium);
    }

    public int getPrice() {
        try {
            return Integer.parseInt(price.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul pret");
        }
        return -1;
    }

    public void setPrice(String price) {
        this.price.setText(price);
    }
}
