package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class RegisterView extends JFrame {
    private JTextField userName = new JTextField(30);
    private JTextField password = new JTextField(30);
    private JButton finish = new JButton("Finish");

    public RegisterView(){
        JPanel user = new JPanel();
        user.setLayout(new BoxLayout(user, BoxLayout.X_AXIS));
        user.add(new JLabel("Username"));
        user.add(userName);

        JPanel pass = new JPanel();
        pass.setLayout(new BoxLayout(pass, BoxLayout.X_AXIS));
        pass.add(new JLabel("Password"));
        pass.add(password);

        JPanel buton = new JPanel();
        buton.setLayout(new FlowLayout());
        buton.add(finish);

        JPanel last = new JPanel();
        last.setLayout(new BoxLayout(last, BoxLayout.Y_AXIS));
        last.add(user);
        last.add(pass);
        last.add(buton);

        this.setContentPane(last);
        this.setTitle("Register Window");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    public String getUserName(){
        return userName.getText();
    }

    public void setUserName(String userName){
        this.userName.setText(userName);
    }

    public String getPassword(){
        return password.getText();
    }

    public void setPassword(String password){
        this.password.setText(password);
    }

    public void addFinishListener(ActionListener mal){
        finish.addActionListener(mal);
    }

    public void reset(){
        userName.setText("");
        password.setText("");
    }
}
