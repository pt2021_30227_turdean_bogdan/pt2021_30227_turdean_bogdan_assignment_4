package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class LogInView extends JFrame {
    private JTextField userName = new JTextField(30);
    private JTextField password = new JTextField(30);
    private JButton login = new JButton("LogIn");
    private JButton register = new JButton("Register");

    public LogInView(){
        JPanel user = new JPanel();
        user.setLayout(new BoxLayout(user, BoxLayout.X_AXIS));
        user.add(new JLabel("Username"));
        user.add(userName);

        JPanel pass = new JPanel();
        pass.setLayout(new BoxLayout(pass, BoxLayout.X_AXIS));
        pass.add(new JLabel("Password"));
        pass.add(password);

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));
        butoane.add(login);
        butoane.add(register);

        JPanel last = new JPanel();
        last.setLayout(new BoxLayout(last, BoxLayout.Y_AXIS));
        last.add(user);
        last.add(pass);
        last.add(butoane);

        this.setContentPane(last);
        this.setTitle("Log In Window");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public String getUserName() {
        return userName.getText();
    }

    public void setUserName(String userName) {
        this.userName.setText(userName);
    }

    public String getPassword() {
        return password.getText();
    }

    public void setPassword(String password) {
        this.password.setText(password);
    }

    public void addLogInListener(ActionListener mal){
        this.login.addActionListener(mal);
    }

    public void addRegisterListener(ActionListener mal){
        this.register.addActionListener(mal);
    }
}
