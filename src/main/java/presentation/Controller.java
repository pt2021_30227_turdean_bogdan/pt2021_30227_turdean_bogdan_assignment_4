package presentation;

import business.*;
import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

public class Controller {
    private AdminView adminView;
    private static DeliveryService deliveryService;
    private LogInView logInView;
    private RegisterView registerView;
    private static HashSet<User> users = new HashSet<>();
    private ClientView clientView;
    private EmployeeView employeeView;
    private static int clientID;

    public Controller(AdminView adminView) {
        this.adminView = adminView;
        this.adminView.addImportAllListener(new ImportAllListener());
        this.adminView.addAddProductListener(new AddProductListener());
        this.adminView.addDeleteProductListener(new DeleteProductListener());
        this.adminView.addEditProductListener(new EditProductListener());
        this.adminView.addCreateCompositeProductListener(new CreateCompositeProductListener());
        this.adminView.addRapord1Listener(new Raport1Lstener());
        this.adminView.addRapord2Listener(new Raport2Lstener());
        this.adminView.addRapord3Listener(new Raport3Lstener());
        this.adminView.addRapord4Listener(new Raport4Lstener());
        deliveryService.printProducts(deliveryService.getMenu(), adminView.getTable());
    }

    public Controller(LogInView logInView){
        this.logInView = logInView;
        deliveryService = DeliveryService.deserializable();
        users = User.deserializable();
        this.logInView.addLogInListener(new LogInListener());
        this.logInView.addRegisterListener(new RegisterListener());

        for(User u: users){
            System.out.println(u.getUsername());
        }
        System.out.println(users.size());
    }

    public Controller(RegisterView registerView){
        this.registerView = registerView;
        this.registerView.addFinishListener(new FinishListener());
    }

    public Controller(ClientView clientView){
        this.clientView = clientView;
        clientView.addViewAllListener(new ViewAllListener());
        clientView.addSearchListener(new SearchListener());
        clientView.addCreateOrderListener(new CreateOrderListener());
    }

    public Controller(EmployeeView employeeView){
        this.employeeView = employeeView;
        employeeView.getArea().repaint();
        for(Order o: deliveryService.getOrders().keySet()){
            String s = "Comanda " + o.getId() + " Facuta de clientul "+ o.getClientId() + " " + o.getDate().toString() + '\n';
            employeeView.setArea(s);
        }
        employeeView.addProcesareListener(new ProcesareListener());
    }

    private class ImportAllListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            //deliveryService.insertAllProducts();
            deliveryService.printProducts(deliveryService.getMenu(), adminView.getTable());
            DeliveryService.serializable(deliveryService);
        }
    }

    private class AddProductListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id = adminView.getId();
            String title = adminView.getName();
            double rating = adminView.getRating();
            int proteins = adminView.getProteins();
            int calories = adminView.getCalories();
            int fats = adminView.getFats();
            int sodium = adminView.getSodium();
            int price = adminView.getPrice();

            if(deliveryService.findById(id) != null){
                JOptionPane.showMessageDialog(adminView, "Id-ul exista");
            }
            else{
                MenuItem item = new BaseProduct(id, title, rating, calories, proteins, fats, sodium, price);
                deliveryService.addProduct(item);
                adminView.reset();
                DeliveryService.serializable(deliveryService);
                HashSet<MenuItem> list = new HashSet<>();
                list.add(deliveryService.findById(id));
                deliveryService.printProducts(list, adminView.getTable());
                JOptionPane.showMessageDialog(adminView, "Adaugat cu sucess");
            }
        }
    }

    private class DeleteProductListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id = adminView.getId();
            deliveryService.deleteProduct(id);
            DeliveryService.serializable(deliveryService);
            deliveryService.printProducts(deliveryService.getMenu(), adminView.getTable());
            adminView.reset();
        }
    }

    private class EditProductListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id = adminView.getId();
            String title = adminView.getName();
            double rating = adminView.getRating();
            int proteins = adminView.getProteins();
            int calories = adminView.getCalories();
            int fats = adminView.getFats();
            int sodium = adminView.getSodium();
            int price = adminView.getPrice();

            if(deliveryService.findById(id) == null){
                JOptionPane.showMessageDialog(adminView, "Id-ul NU exista");
            }
            else{
                MenuItem item = new BaseProduct(id, title, rating, calories, proteins, fats, sodium, price);
                deliveryService.editProduct(id, title, rating, calories, proteins, fats, sodium, price);
                adminView.reset();
                DeliveryService.serializable(deliveryService);
                JOptionPane.showMessageDialog(adminView, "Editat cu sucess");
                deliveryService.printProducts(deliveryService.getMenu(), adminView.getTable());
            }
        }
    }

    private class CreateCompositeProductListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

        }
    }

    private class LogInListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String userName = logInView.getUserName();
            String password = logInView.getPassword();

            for(User u: users){
                if(userName.compareTo(u.getUsername()) == 0 && password.compareTo(u.getParola()) == 0){
                    clientID = u.getId();
                    if(u.getRole().compareTo(Role.ADMIN) == 0){
                        AdminView view = new AdminView();
                        view.setSize(1000, 800);
                        view.setVisible(true);
                        Controller controller = new Controller(view);
                    }
                    else{
                        if(u.getRole().compareTo(Role.CLIENT) == 0){
                            ClientView view = new ClientView();
                            view.setSize(1000, 800);
                            view.setVisible(true);
                            Controller controller = new Controller(view);
                        }
                        else{
                            EmployeeView view = new EmployeeView();
                            view.setSize(1000, 800);
                            view.setVisible(true);
                            Controller controller = new Controller(view);
                        }
                    }
                    break;
                }
            }
        }
    }

    private class RegisterListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            RegisterView registerView = new RegisterView();
            Controller controller = new Controller(registerView);
            registerView.setVisible(true);
            registerView.setSize(500, 250);

        }
    }

    private class FinishListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String userName = registerView.getUserName();
            String password = registerView.getPassword();

            if(userName.compareTo("") != 0 && password.compareTo("") != 0) {
                int id = 0;
                for (int c = 0; c < userName.length(); c++) {
                    id += userName.toCharArray()[c];
                }
                User user = new User(id, userName, password, Role.CLIENT);
                users.add(user);
                User.serializable(users);
                System.out.println(users.size());
                for (User u : users) {
                    System.out.println(u.getUsername());
                }
                registerView.reset();
            }
            else{
                JOptionPane.showMessageDialog(registerView, "Introduceti date");
            }
        }
    }

    private class ViewAllListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            deliveryService.printProducts(deliveryService.getMenu(), clientView.getTable());
        }
    }

    private class SearchListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int id = clientView.getId();
            String title = clientView.getName();
            double rating = clientView.getRating();
            int proteins = clientView.getProteins();
            int calories = clientView.getCalories();
            int fats = clientView.getFats();
            int sodium = clientView.getSodium();
            int price = clientView.getPrice();
            HashSet<MenuItem> list = deliveryService.searchProducts(title, rating, calories, proteins, fats, sodium, price);
            deliveryService.printProducts(list, clientView.getTable());
        }
    }

    private class CreateOrderListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int[] selectedRows = clientView.getTable().getSelectedRows();
            HashSet<MenuItem> selectedItems = new HashSet<>();
            for(int i = 0; i < selectedRows.length; i++){
                int id = Integer.parseInt(String.valueOf(clientView.getTable().getValueAt(i, 0)));
                String title = String.valueOf(clientView.getTable().getValueAt(i, 1));
                double rating = Double.parseDouble(String.valueOf(clientView.getTable().getValueAt(i, 2)));
                int calories = Integer.parseInt(String.valueOf(clientView.getTable().getValueAt(i, 3)));
                int proteins = Integer.parseInt(String.valueOf(clientView.getTable().getValueAt(i, 4)));
                int fats = Integer.parseInt(String.valueOf(clientView.getTable().getValueAt(i, 5)));
                int sodium = Integer.parseInt(String.valueOf(clientView.getTable().getValueAt(i, 6)));
                int price = Integer.parseInt(String.valueOf(clientView.getTable().getValueAt(i, 7)));
                MenuItem menuItem = new BaseProduct(id, title, rating, calories, proteins, fats, sodium, price);
                for(MenuItem m: deliveryService.getMenu()){
                    if(m.getId() == id){
                        m.setTimes();
                    }
                }
                menuItem.setTimes();
                selectedItems.add(menuItem);
            }
            Order order = new Order(deliveryService.getOrders().size(), clientID);
            for(User u: users){
                if(u.getId() == clientID){
                    u.setTimes();
                }
            }
            deliveryService.createOrder(order, selectedItems);
            EmployeeView view = new EmployeeView();
            view.update(order);
            view.getArea().repaint();
            for(Order o: deliveryService.getOrders().keySet()){
                String s = "Comanda " + o.getId() + " Facuta de clientul "+ o.getClientId() + " " + o.getDate().toString() + '\n';
                view.setArea(s);
            }
            deliveryService.createBill(order, selectedItems);
            DeliveryService.serializable(deliveryService);
        }
    }

    private class ProcesareListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            employeeView.getArea().setText("");
            deliveryService.getOrders().clear();
            DeliveryService.serializable(deliveryService);
        }
    }

    private class Raport1Lstener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int h1, h2;
            h1 = adminView.getOra1();
            h2 = adminView.getOra2();
            deliveryService.raportOre(h1, h2);
            JOptionPane.showMessageDialog(adminView, "Success");
        }
    }

    private class Raport2Lstener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int times = adminView.getTimes();
            deliveryService.raportProdus(times);
            JOptionPane.showMessageDialog(adminView, "Success");
        }
    }

    private class Raport3Lstener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int times = adminView.getTimes();
            int price = adminView.getOra2();

            deliveryService.raportClient(times, price, users);
            JOptionPane.showMessageDialog(adminView, "Success");
        }
    }

    private class Raport4Lstener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int zi = adminView.getOra1();
            deliveryService.raportZi(zi);
            JOptionPane.showMessageDialog(adminView, "Success");
        }
    }
}
