package presentation;

import business.Order;

import javax.swing.*;
import java.awt.event.ActionListener;

public class EmployeeView extends JFrame implements Observer{
    private JTextArea area = new JTextArea(1000, 1000);
    private JButton buton = new JButton("Procesare");

    public EmployeeView(){
        JPanel last = new JPanel();
        last.setLayout(new BoxLayout(last, BoxLayout.Y_AXIS));
        last.add(buton);
        last.add(area);

        this.setContentPane(last);
        this.setTitle("Employee Window");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    public void addProcesareListener(ActionListener mal){
        buton.addActionListener(mal);
    }

    public JTextArea getArea() {
        return area;
    }

    public void setArea(String s) {
        this.area.append(s);
    }

    @Override
    public void update(Order order) {
        this.setVisible(true);
        this.setSize(1000, 800);
        JOptionPane.showMessageDialog(this, "Comanda noua facuta de: " + order.getClientId());
    }
}
