package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AdminView extends JFrame {
    private JTextField id = new JTextField(10);
    private JTextField title = new JTextField(100);
    private JTextField rating = new JTextField(10);
    private JTextField calories = new JTextField(10);
    private JTextField proteins = new JTextField(10);
    private JTextField fats = new JTextField(10);
    private JTextField sodium = new JTextField(10);
    private JTextField price = new JTextField(10);

    private JButton importAll = new JButton("Import all");
    private JButton add = new JButton("Add product");
    private JButton delete = new JButton("Delete product");
    private JButton edit = new JButton("Edit product");
    private JButton create = new JButton("Create composite product");

    private JTextField h1 = new JTextField(10);
    private JTextField h2 = new JTextField(10);
    private JButton raport1 = new JButton("Raport1");
    private JButton raport2 = new JButton("Raport2");
    private JButton raport3 = new JButton("Raport3");
    private JButton raport4 = new JButton("Raport4");

    private JTextField times = new JTextField(10);

    private JTable table;

    public AdminView() {
        JPanel panelTitlu = new JPanel();
        panelTitlu.add(new JLabel("ADMIN"));
        panelTitlu.setLayout(new FlowLayout());

        JPanel butoane1 = new JPanel();
        butoane1.setLayout(new BoxLayout(butoane1, BoxLayout.X_AXIS));
        butoane1.add(new JLabel("ID"));
        butoane1.add(id);

        JPanel butoane2 = new JPanel();
        butoane2.setLayout(new BoxLayout(butoane2, BoxLayout.X_AXIS));
        butoane2.add(new JLabel("Title"));
        butoane2.add(title);

        JPanel butoane3 = new JPanel();
        butoane3.setLayout(new BoxLayout(butoane3, BoxLayout.X_AXIS));
        butoane3.add(new JLabel("Rating"));
        butoane3.add(rating);

        JPanel butoane4 = new JPanel();
        butoane4.setLayout(new BoxLayout(butoane4, BoxLayout.X_AXIS));
        butoane4.add(new JLabel("Calories"));
        butoane4.add(calories);

        JPanel butoane5 = new JPanel();
        butoane5.setLayout(new BoxLayout(butoane5, BoxLayout.X_AXIS));
        butoane5.add(new JLabel("Proteins"));
        butoane5.add(proteins);

        JPanel butoane6 = new JPanel();
        butoane6.setLayout(new BoxLayout(butoane6, BoxLayout.X_AXIS));
        butoane6.add(new JLabel("Fats"));
        butoane6.add(fats);

        JPanel butoane7 = new JPanel();
        butoane7.setLayout(new BoxLayout(butoane7, BoxLayout.X_AXIS));
        butoane7.add(new JLabel("Sodium"));
        butoane7.add(sodium);

        JPanel butoane8 = new JPanel();
        butoane8.setLayout(new BoxLayout(butoane8, BoxLayout.X_AXIS));
        butoane8.add(new JLabel("Price"));
        butoane8.add(price);

        JPanel timesP = new JPanel();
        timesP.setLayout(new BoxLayout(timesP, BoxLayout.X_AXIS));
        timesP.add(new JLabel("Times"));
        timesP.add(times);

        JPanel panelTabel = new JPanel();
        panelTabel.setLayout(new FlowLayout());
        String data[][] = new String [100000][8];
        String column[] = {"ID", "Title", "Rating", "Calories", "Proteins", "Fats", "Sodium", "Price"};
        table = new JTable(data, column);
        JScrollPane pane = new JScrollPane(table);
        panelTabel.add(pane);

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.X_AXIS));
        butoane.add(importAll);
        butoane.add(add);
        butoane.add(delete);
        butoane.add(edit);
        butoane.add(create);
        butoane.add(raport1);
        butoane.add(raport2);
        butoane.add(raport3);
        butoane.add(raport4);

        JPanel ora1 = new JPanel();
        ora1.setLayout(new BoxLayout(ora1, BoxLayout.X_AXIS));
        ora1.add(new JLabel("H1/Zi"));
        ora1.add(h1);

        JPanel ora2 = new JPanel();
        ora2.setLayout(new BoxLayout(ora2, BoxLayout.X_AXIS));
        ora2.add(new JLabel("H2/Price"));
        ora2.add(h2);

        JPanel last = new JPanel();
        last.setLayout(new BoxLayout(last, BoxLayout.Y_AXIS));
        last.add(panelTitlu);
        last.add(butoane1);
        last.add(butoane2);
        last.add(butoane3);
        last.add(butoane4);
        last.add(butoane5);
        last.add(butoane6);
        last.add(butoane7);
        last.add(butoane8);
        last.add(timesP);
        last.add(ora1);
        last.add(ora2);
        last.add(butoane);
        last.add(panelTabel);

        this.setContentPane(last);
        this.setTitle("Admin Window");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public void addImportAllListener(ActionListener mal){
        importAll.addActionListener(mal);
    }

    public void addAddProductListener(ActionListener mal){
        add.addActionListener(mal);
    }

    public void addDeleteProductListener(ActionListener mal){
        delete.addActionListener(mal);
    }

    public void addEditProductListener(ActionListener mal){
        edit.addActionListener(mal);
    }

    public void addCreateCompositeProductListener(ActionListener mal){
        create.addActionListener(mal);
    }

    public int getId() {
        try {
            return Integer.parseInt(id.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti un numar in campul ID");
        }
        return -1;
    }

    public void setId(String id) {
        this.id.setText(id);
    }

    public String getName() {
        return title.getText();
    }

    public void setName(String title) {
        this.title.setText(title);
    }

    public double getRating() {
        try{
            return Double.parseDouble(rating.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti un numar pentru campl rating");
        }
        return -1;
    }

    public void setRating(String rating) {
        this.rating.setText(rating);
    }

    public int getCalories() {
        try {
            return Integer.parseInt(calories.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul calorii");
        }
        return -1;
    }

    public void setCalories(String calories) {
        this.calories.setText(calories);
    }

    public int getProteins() {
        try {
            return Integer.parseInt(proteins.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul proteine");
        }
        return -1;
    }

    public void setProteins(String proteins) {
        this.proteins.setText(proteins);
    }

    public int getFats() {
        try {
            return Integer.parseInt(fats.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul grasimi");
        }
        return -1;
    }

    public void setFats(String fats) {
        this.fats.setText(fats);
    }

    public int getSodium() {
        try {
            return Integer.parseInt(sodium.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul sodiu");
        }
        return -1;
    }

    public void setSodium(String sodium) {
        this.sodium.setText(sodium);
    }

    public int getPrice() {
        try {
            return Integer.parseInt(price.getText());
        }
        catch (NumberFormatException e){
            //JOptionPane.showMessageDialog(this, "Introduceti numar pentru campul pret");
        }
        return -1;
    }

    public void setPrice(String price) {
        this.price.setText(price);
    }

    public void reset(){
        id.setText("");
        title.setText("");
        rating.setText("");
        calories.setText("");
        proteins.setText("");
        fats.setText("");
        sodium.setText("");
        price.setText("");
    }

    public void setOra1(String h1){
        this.h1.setText(h1);
    }

    public int getOra1(){
        return Integer.parseInt(this.h1.getText());
    }

    public void setOra2(String h2){
        this.h2.setText(h2);
    }

    public int getOra2(){
        return Integer.parseInt(this.h2.getText());
    }

    public void addRapord1Listener(ActionListener mal){
        raport1.addActionListener(mal);
    }

    public void addRapord2Listener(ActionListener mal){
        raport2.addActionListener(mal);
    }

    public void addRapord3Listener(ActionListener mal){
        raport3.addActionListener(mal);
    }

    public void addRapord4Listener(ActionListener mal){
        raport4.addActionListener(mal);
    }

    public int getTimes(){
        return Integer.parseInt(times.getText());
    }

    public void setTimes(String s){
        this.times.setText(s);
    }
}
