package business;

import java.io.Serializable;

public enum Role implements Serializable {
    ADMIN("Admin"), EMPLOYEE("Employee"), CLIENT("CLient");

    private final String roleType;

    Role(String roleType){
        this.roleType = roleType;
    }

    public String getRoleType(){
        return roleType;
    }

    public static Role setRole(String roleType){
        switch (roleType){
            case "Admin":
                return Role.ADMIN;
            case "Employee":
                return Role.EMPLOYEE;
            case "Client":
                return Role.CLIENT;
            default:
                return null;
        }
    }
}
