package business;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Serializable {
    private ArrayList<MenuItem> items;
    public CompositeProduct(int id, String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
        super(id, title, rating, calories, proteins, fats, sodium, price);
        items = new ArrayList<>();
    }

    @Override
    public int computePrice() {
        int price = 0;
        for(MenuItem m: this.items){
            price += m.computePrice();
        }
        return price;
    }
}
