package business;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {
    private int id;
    private int clientId;
    private Date date;
    private int price;

    public Order(int id, int clientId) {
        this.id = id;
        this.clientId = clientId;
        this.date = new Date();
        price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void computePrice(int price){
        this.price = price;
    }

    public int getPrice(){
        return this.price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && clientId == order.clientId && Objects.equals(date, order.date);
    }

    @Override
    public int hashCode() {
        return id + clientId;
    }


}
