package business;

import javax.swing.*;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable {

    private Map<Order, HashSet<MenuItem>> orders = new HashMap<>();
    private HashSet<MenuItem> menu = new HashSet<>();
    private HashSet<BaseProduct> baseProducts = new HashSet<>();

    public boolean isWellFormed(){
        if(baseProducts != null)
            return true;
        return false;
    }

    public HashSet<BaseProduct> getBaseProducts() {
        return baseProducts;
    }

    public void setBaseProducts(HashSet<BaseProduct> baseProducts) {
        this.baseProducts = baseProducts;
    }

    public Map<Order, HashSet<MenuItem>> getOrders() {
        return orders;
    }

    public void setOrders(Map<Order, HashSet<MenuItem>> orders) {
        this.orders = orders;
    }

    public HashSet<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(HashSet<MenuItem> menu) {
        this.menu = menu;
    }
    /**
     * @param product menu
     * @pre product!=null
     * @post menu.contains(product)
     **/
    @Override
    public void addProduct(MenuItem product) {
        assert (product != null);
        this.menu.add(product);
        assert (menu.contains(product));
        assert (isWellFormed());
    }

    /**
     *
     * @param id
     * @pre id>=0
     * @post !menu.contains(deletedItem)
     */
    @Override
    public void deleteProduct(int id) {
        assert (id>=0);
        MenuItem deletedItem = findById(id);
        this.menu.remove(deletedItem);
        assert (!menu.contains(deletedItem));
        assert (isWellFormed());
    }


    /**
     *
     * @param id
     * @param title
     * @param rating
     * @param calories
     * @param proteins
     * @param fats
     * @param sodium
     * @param price
     * @pre id>=0
     * @pre title!=null
     * @pre calories>0
     * @pre proteins>0
     * @pre fats>0
     * @pre sodium>0
     * @pre price>0
     */
    @Override
    public void editProduct(int id, String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
        assert (id >= 0);
        assert (title != null);
        assert (calories > 0);
        assert (proteins > 0);
        assert (fats > 0);
        assert (sodium > 0);
        assert (price > 0);
        MenuItem item = findById(id);
        if(title.compareTo("") != 0)
            item.setTitle(title);
        if(proteins != -1)
            item.setProteins(proteins);
        if(sodium != -1)
            item.setSodium(sodium);
        if(price != -1)
            item.setPrice(price);
        if(rating != -1)
            item.setRating(rating);
        if(fats != -1)
            item.setFats(fats);
        if(calories != -1)
            item.setCalories(calories);
        assert (isWellFormed());
    }

    /**
     *
     * @param order
     * @param items
     * @pre order!=null
     * @post items!=null
     */
    @Override
    public void createOrder(Order order, HashSet<MenuItem> items) {
        assert(order != null);
        int total = 0;
        for(MenuItem i: items){
            total += i.computePrice();
        }
        order.computePrice(total);
        this.orders.put(order, items);
        assert (items != null);
        assert (isWellFormed());
    }

    /**
     *
     * @param order
     * @param items
     * @pre order!=null
     *
     */
    @Override
    public void createBill(Order order, HashSet<MenuItem> items) {
        assert(order != null);

        try {
            FileWriter fw = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\bill.txt");
            fw.write("Comanda clientului " + order.getClientId() + ": \n");
            fw.write(order.getDate().toString()+'\n');
            for(MenuItem i: items){
                fw.write(i.getTitle() + "\n");
            }
            int total = 0;
            for(MenuItem i: items){
                total += i.computePrice();
            }
            fw.write("Total: " + total);
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        assert (isWellFormed());
    }

    public void insertAllProducts() {
        try {
            List<String[]> list = Files.lines(Paths.get("products.csv")).skip(1).map(line -> line.split(",")).collect(Collectors.toList());
            int id = 0;
            String title = null;
            double rating = 0.0;
            int calories = 0;
            int proteins = 0;
            int fats = 0;
            int sodium = 0;
            int price = 0;
            for(String[] linie: list){
                int i = 0;
                for(String coloana: linie){
                    switch(i){
                        case(0):
                           title = coloana;
                           break;
                        case(1):
                            rating = Double.parseDouble(coloana);
                            break;
                        case(2):
                            calories = Integer.parseInt(coloana);
                            break;
                        case(3):
                            proteins = Integer.parseInt(coloana);
                            break;
                        case(4):
                            fats = Integer.parseInt(coloana);
                            break;
                        case(5):
                            sodium = Integer.parseInt(coloana);
                            break;
                        case(6):
                            price = Integer.parseInt(coloana);
                            break;
                        default:
                            break;
                    }
                    i++;
                }
                boolean ok = true;
                for(BaseProduct bp: baseProducts){
                    if(bp.getTitle().compareTo(title) == 0){
                        ok = false;
                    }
                }
                if(ok == true) {
                    id++;
                    BaseProduct baseProduct = new BaseProduct(id, title, rating, calories, proteins, fats, sodium, price);
                    baseProducts.add(baseProduct);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(BaseProduct bp: baseProducts){
            addProduct(bp);
        }
    }

    public HashSet<MenuItem> searchProducts(String title, double rating, int calories, int proteins, int fats, int sodium, int price){
        HashSet<MenuItem> list = menu;
        if(title != null){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getTitle().contains(title)).collect(Collectors.toSet());
        }
        if(rating != -1){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getRating() == rating).collect(Collectors.toSet());
        }
        if(calories != -1){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getCalories() == calories).collect(Collectors.toSet());
        }
        if(proteins != -1){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getProteins() == proteins).collect(Collectors.toSet());
        }
        if(fats != -1){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getFats() == fats).collect(Collectors.toSet());
        }
        if(sodium != -1){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getSodium() == sodium).collect(Collectors.toSet());
        }
        if(price != -1){
            list = (HashSet<MenuItem>) list.stream().filter(p -> p.getPrice() == price).collect(Collectors.toSet());
        }

        return list;
    }

    public void printProducts(HashSet<MenuItem> menu, JTable table){
        int line = 0;
        for(int i = 0; i < 100000; i++){
            for(int j = 0; j < 8; j++){
                table.setValueAt("", i, j);
            }
        }
        for(MenuItem item: menu){
            for(int i = 0; i < 8; i++){
                switch (i){
                    case(0):
                        table.setValueAt(String.valueOf(item.getId()), line, i);
                        break;
                    case(1):
                        table.setValueAt(item.getTitle(), line, i);
                        break;
                    case(2):
                        table.setValueAt(String.valueOf(item.getRating()), line, i);
                        break;
                    case (3):
                        table.setValueAt(String.valueOf(item.getCalories()), line, i);
                        break;
                    case(4):
                        table.setValueAt(String.valueOf(item.getProteins()), line, i);
                        break;
                    case (5):
                        table.setValueAt(String.valueOf(item.getFats()), line, i);
                        break;
                    case (6):
                        table.setValueAt(String.valueOf(item.getSodium()), line, i);
                        break;
                    case (7):
                        table.setValueAt(String.valueOf(item.getPrice()), line, i);
                        break;
                    default:
                        break;
                }
            }
            line++;
        }
    }

    public MenuItem findById(int id){
        for(MenuItem m: this.menu){
            if(m.getId() == id)
                return m;
        }
        return null;
    }

    public static void serializable(DeliveryService deliveryService){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\delivery.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(deliveryService);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DeliveryService deserializable(){
        try {
            FileInputStream fileInputStream = new FileInputStream("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\delivery.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            DeliveryService deliveryService = (DeliveryService) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return deliveryService;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void raportOre(int o1, int o2){
        try {
            FileWriter fileWriter = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\raport1.txt");
            Set<Order> orders = this.orders.keySet().stream().filter(p -> p.getDate().getHours() >= o1).filter(p -> p.getDate().getHours() <=o2).collect(Collectors.toSet());
            for(Order o: orders){
                String s = "Comanda " + o.getId() + " facuta de clientul " + o.getClientId() + " " + o.getDate().toString();
                fileWriter.write(s + '\n');
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void raportProdus(int times){
        try {
            FileWriter fileWriter = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\raport2.txt");
            List<MenuItem> items = new ArrayList<>();
            items = menu.stream().filter(p -> p.getTimes() >= times).collect(Collectors.toList());
            for(MenuItem m: items){
                fileWriter.write(m.getTitle() + "\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void raportClient(int times, int price, HashSet<User> users){
        try {
            FileWriter fileWriter = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\raport3.txt");
            List<User> utilizatori = users.stream().filter(p -> p.getRole().equals(Role.CLIENT)).filter(p -> p.getTimes() >= times).collect(Collectors.toList());
            List<User> result = new ArrayList<>();
            for(User u: utilizatori){
                Map<Order, HashSet<MenuItem>> comenzileUtilizatorului = orders.entrySet().stream().filter(p -> p.getKey().getPrice() >= price).collect(Collectors.toMap(a -> a.getKey(), a -> a.getValue()));
                if(comenzileUtilizatorului.size() > 0)
                    fileWriter.write(u.getUsername() + '\n');
            }

            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void raportZi(int zi){
        try {
            FileWriter fileWriter = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\raport4.txt");
            Map<Order, HashSet<MenuItem>> comenziZI = orders.entrySet().stream().filter(p -> p.getKey().getDate().getDay() == zi).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            HashSet<MenuItem> produse = new HashSet<>();
            for(Order o: comenziZI.keySet()){
                produse.addAll(comenziZI.get(o));
            }
            for(MenuItem m: produse){
                fileWriter.write(m.getTitle() + " " + m.getTimes() + '\n');
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
