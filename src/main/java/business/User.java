package business;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;

public class User implements Serializable {
    private int id;
    private String username;
    private String parola;
    private Role role;
    private int times;

    public User(int id, String username, String parola, Role role) {
        this.id = id;
        this.username = username;
        this.parola = parola;
        this.role = role;
        times = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public static void serializable(HashSet<User> users){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\user.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(users);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static HashSet<User> deserializable(){
        try {
            FileInputStream fileInputStream = new FileInputStream("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_4\\src\\main\\java\\user.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            HashSet<User> users = (HashSet<User>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return users;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes() {
        this.times++;
    }
}
