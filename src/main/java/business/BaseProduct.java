package business;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {

    public BaseProduct(int id, String title, double rating, int calories, int proteins, int fats, int sodium, int price) {
        super(id, title, rating, calories, proteins, fats, sodium, price);
    }

    @Override
    public int computePrice() {
        return getPrice();
    }


}
