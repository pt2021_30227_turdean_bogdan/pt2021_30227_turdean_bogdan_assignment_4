package business;

import java.util.ArrayList;
import java.util.HashSet;

public interface IDeliveryServiceProcessing {
    /**
     * @param product menu
     * @pre product!=null
     * @post menu.contains(product)
     **/
    public void addProduct(MenuItem product);

    /**
     *
     * @param id
     * @pre id>=0
     * @post !menu.contains(deletedItem)
     */
    public void deleteProduct(int id);

    /**
     *
     * @param id
     * @param title
     * @param rating
     * @param calories
     * @param proteins
     * @param fats
     * @param sodium
     * @param price
     * @pre id>=0
     * @pre title!=null
     * @pre calories>0
     * @pre proteins>0
     * @pre fats>0
     * @pre sodium>0
     * @pre price>0
     */
    public void editProduct(int id, String title, double rating, int calories, int proteins, int fats, int sodium, int price);

    /**
     *
     * @param order
     * @param items
     * @pre order!=null
     * @post items!=null
     */
    public void createOrder(Order order, HashSet<MenuItem> items);

    /**
     *
     * @param order
     * @param items
     * @pre order!=null
     *
     */
    public void createBill(Order order, HashSet<MenuItem> items);

}
